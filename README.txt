
Copyright 2008 Joshua Benner

Description
-----------
Project Maintainers adds a subtab when editing a project called 'Maintainers',
which allows anyone with permission to edit a project node to specify any
number of users as "Maintainers" for a project. Then, any user specified as a
maintainer will then appear in the assign to dropdown when creating/editing an
issue node or commenting on an issue node.

Features
--------
* Specify a unique list of maintainers for every project
* Any user with 'assign to maintainer' permission will see all maintainers in
  assign to dropdown when creating/editing or commenting on an issue node
  
Prerequisites
-------------
* Drupal 5
* Project
* Project Issue (only tested with 2.0)

Installation
------------
Copy the project_maintainers folder to the appropriate modules folder, such as
sites/all/modules, then enable the module in Site Building -> Modules.

Author
------
Joshua Bener (joshbenner /at\ gmail.com)

Thanks to my employer, Rock River Star (www.rockriverstar.com)